# Fearless Concurrency
* Rust uses ownership and borrowing through the type system to make concurrency safer
  * Doesn't aim to prevent deadlocks
  * Aims to minimize data races
* For our purposes, consider both concurrency and parallelism when talking about concurrency
* [Seven concurrency models in seven weeks](https://pragprog.com/titles/pb7con/seven-concurrency-models-in-seven-weeks/) - A book about concurrency covered previously in SWRG 
* Unlike high level languages that havea 'runtime' that offers a particular concurrency model - some are even dogmatic and stick to only model, rust provides basic concurrency primitives in the standard library and relegates other models to other crates
* This means that the default threading model is OS thread as opposed to 'green' threads
* What we'll cover those in this talk:
  - Threading
  - Message passing concurrency (Share by communicating)
  - Shared-state concurrency (Communicate by sharing)
  - `Sync` and `Send` traits - extend concurrency guarantess to user defined types
* We won't cover `Async`/`Await` or other concurrency models. We can go over those in a separate meeting if people are interested

# Threading
* Rust uses 1:1 thread model where a Rust thread is the same as an OS thread
  - There are other threading models such as green threads, which do a M:N ratio of green threads vs OS threads. This needs a runtime that is present in the binary and involves more overheads. There are crates that implement this if needed
  - Rust also provides a language mechanism for asynchronous programming calles `async` & `await`. This, along with the `Future` trait is becoming another way to write performant asynchronous code. See this talk ([Rust's journey into Async/Await](https://www.youtube.com/watch?v=lJ3NC-R3gSI)) for history and intro
* On to live coding

# Shared memory concurrency
* Communicate with other threads by sharing memory
* Shared memory is protected by `Mutex`
* `Mutex` is guarded by scope
* How do we *share* the mutex across different threads?
  * `Rc` vs `Arc`
* `Mutex` and interior mutability - like in `RefCell`
* `Arc<Mutex<T>>` is a multi-threaded equivalent of `Rc<RefCell<T>>` 
* On to live coding

# Message passing concurrency
* Like in the Go language - threads communicate with each other by passing messages
   "Do not communicate by sharing memory; instead, share memory by communicating"
* Rust standard library provides a *channel*. A *channel* has two halves:
  - `Sender`, and
  - `Receiver`
  - `Sender` or `Receiver` can be *closed* or *hung up* on if they are `Drop`ped
* We'll be looking at a *mpsc* channel
  - Multiple producer
  - Single consumer
* Multiple consumer version is not provided in standard library. Some crates do similar thigs: bus, multiqueue, etc.
* On to live coding
  
# Extending concurrency guarantees to user defined dat types
* Most of the above concurrency features are not baked into the language, but the standard library
  - Except for the traits `Send` and `Sync` in the `std::marker` module. These are part of the language
* `Send` trait - indicates that ownership can be *transferred* between threads
  - Most Rust types are `Send`able. Some exceptions include `Rc`
* `Sync` trait - indicates that it is safe to be *referenced* from multiple threads
  - Primitive types are `Sync`. Exceptions include `Rc`, `RefCell` and related `Cell` types because they are not thread-safe implementations.
  - As can be guessed, `Mutex` is `Sync`
* Types made entirely of `Send` and `Sync` are automatically `Send` and `Sync` themselves respectively.
* Manually implementing `Send` and `Sync` is done using Unsafe, which will be covered in a later chapter.

# Summary
* Rust uses ownership and borrowing (i.e. the type checker) to make concurrency safe
* By default, rust uses 1:1 threading (native threading) and provides language level features to make asynchronous programming easy (`async`, `await`)
* There are very minimal things for threading that are part of the language itself - namely the `Sync` and `Send` traits. Rest are crates (native threading is in standard library and other green threads and actors and other concurrency models are in third party crates)
* Rust standard library provides two forms of concurrency
  * Traditional shared-memory concurrency using `Mutex`
  * Channel based concurrency using `mpsc`
* Data types can be referenced or moved to other threads based on implementations of `Sync` and `Send` traits
  * These traits are automatically generated for user defined data types if the consituties themselves implement them. Rust primitive types implement these
  * They can be manually implemented but that would be an Unsafe topic.

# Resources for adjacent topics
* A concurrency book from a past SWRG selection - [Seven concurrency models in seven weeks](https://pragprog.com/titles/pb7con/seven-concurrency-models-in-seven-weeks/)
* Rust Async/Await
  - [Talk - Rust's journey into Async/Await](https://www.youtube.com/watch?v=lJ3NC-R3gSI) 
  - [Talk - The Talk you've been Await-ing for](https://www.youtube.com/watch?v=NNwK5ZPAJCk)