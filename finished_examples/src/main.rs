use std::thread;
use std::time::Duration;

use std::sync::Mutex;
use std::sync::Arc;

use std::sync::mpsc;

fn main() {
    // Message passing concurrency
    // Note: There's also sync_channel that can be used to create a size-bounded
    // channel. In that case, sender.send will block if channel is full
    let (sender, receiver) = mpsc::channel();
    let mut logs = vec![];

    let sender_clone = sender.clone();
    let handle0 = thread::spawn(move || {
        for i in 0..5 {
            thread::sleep(Duration::from_millis(50));
            sender.send(format!("Message {} from thread0", i)).unwrap();
        }
    });

    let handle1 = thread::spawn(move || {
        for i in 0..5 {
            thread::sleep(Duration::from_millis(55));
            sender_clone.send(format!("Message {} from thread1", i)).unwrap();
        }
    });

    handle0.join().unwrap();
    handle1.join().unwrap();

    for message in receiver {
        logs.push(message);
    }

    println!("Logs: {:?}", logs);
}

fn _main2() {
    // Shared memory concurrency
    let logs = Arc::new(Mutex::new(vec![]));

    let logs_clone0 = Arc::clone(&logs);
    let handle0 = thread::spawn(move || {
        let mut logs = logs_clone0.lock().unwrap();
        for i in 0..5 {
            logs.push(format!("Message {} from thread0", i));
        }
    });
    
    let logs_clone1 = Arc::clone(&logs);
    let handle1 = thread::spawn(move || {
        let mut logs = logs_clone1.lock().unwrap();
        for i in 0..10 {
            logs.push(format!("Message {} from thread1", i));
        }
    });

    handle0.join().unwrap();
    handle1.join().unwrap();
    println!("Logs: {:?}", logs.lock().unwrap());
}

fn _main1() {
    // Threads
    let shared_string = String::from("This is a message");

    // Note: there is also a builder based interface to create
    // a thread that allows to tweak some thread parameters
    let handle0 = thread::spawn(move || {
        for i in 0..5 {
            thread::sleep(Duration::from_millis(50));
            println!("Message {} from thread 0: {}", i, shared_string);
        }
    });

    let handle1 = thread::spawn(|| {
        for i in 0..10 {
            thread::sleep(Duration::from_millis(50));
            println!("Message {} from thread 1", i);
        }
    });

    handle0.join().unwrap();
    handle1.join().unwrap();
    // Note that if you uncomment the below line, it won't compile anymore
    // This is as we can't yet share the shared_string across threads
    // We'll figure out two ways of doing this: shared memory concurrency and
    // message passing concurrency

    // println!("{}", shared_string);
}

fn _main0() {
    // use case to build towards: concurrent logging mechanism
    let mut logs = vec![];

    // assume this is a different thread
    {
        logs.push("Message from thread0");
    }

    // assume this is a different thread
    {
        logs.push("Message from thread1");
    }

    println!("Logs: {:?}", logs);
}